<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();
Route::get('/', 'MasterController@home');
Route::get('/kecamatan/{periode}/{citycode}', 'MasterController@kecamatan');
Route::get('/kelurahan/{periode}/{citycode}/{distcode}', 'MasterController@kelurahan');


// Route::group(['middleware' => 'customAuth'], function () {
//     Route::get('/', 'ReportsController@index');
//     Route::get('/dashboard', 'ReportsController@dashboard')->name('dashboard') ;
//     Route::get('/report', 'ReportsController@report')->name('report');
//     Route::get('/toWH', 'ReturnController@toWH')->name('towh');
//     // Route::get('/toHO', 'ReturnController@toHO')->name('toho');
//     Route::get('/toBilling', 'ReturnController@toBilling')->name('tobilling');
//     Route::get('/recBilling', 'ReceivedController@recBilling')->name('recbilling');
// });

Route::get('/data/panel/kab', 'MasterController@getPanelKab');
Route::get('/data/panel/kec', 'MasterController@getPanelKec');
Route::get('/data/panel/kel', 'MasterController@getPanelKel');
Route::get('/data/kab', 'MasterController@getKabupaten');
Route::get('/data/kec', 'MasterController@getKecamatan');
Route::get('/data/kel', 'MasterController@getKelurahan');