<?php
namespace App\Helper;

use Illuminate\Support\Facades\DB;

class HelperApi {
    public static function get_kab($periode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbast/".$periode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_kec($periode, $citykode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbast/".$periode."/".$citykode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_kel($periode, $citykode, $distcode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbast/".$periode."/".$citykode."/".$distcode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_panel_kab($periode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbastttl/".$periode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_panel_kec($periode, $citykode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbastttl/".$periode."/".$citykode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }

    public static function get_panel_kel($periode, $citykode, $distcode) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://202.138.229.86/apibansos/data/dashbastttl/".$periode."/".$citykode."/".$distcode,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
                'x-token: 00043eb6617434cc5f357bbf692e53b3'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return json_decode($response, TRUE);
        }
    }
}