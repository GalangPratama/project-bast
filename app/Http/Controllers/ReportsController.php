<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use ApiCurl;
 
class ReportsController extends Controller
{
    protected $layout = "master.template";

    public function index(){
      return view('welcome');
    }
  
    public function dashboard(){
     return view('welcome');
    }

    public function report(){
     return view('content.report');
    }

    public function ApiDEST(){
      $data = ApiCurl::getDest();
      return response($data);
    }

    public function ApiReports(Request $request){
      $tglawal = $request->input('from_datereport');
      $tglakhir = $request->input('to_datereport');
      $dest = $request->input('select_dest');
      $status = $request->input('select_ho');
      
      $data = ApiCurl::getReport($tglawal, $tglakhir, $dest, $status);

      return DataTables::of($data)
      ->addColumn('action', function($data){
          $button = '<button type="button" name="info" id="'.$data['hawb_no'].'" class="infoReport btn btn-default btn-sm"><i class="ti-info"></i></button>';
          return $button;
      })
      ->rawColumns(['action'])
      ->make(true);
      
      return response($data);
    }

    public function ApiAWB(Request $request){
      $awb = $request->input('awb');
      $data = ApiCurl::getAwb($awb);
      return response($data);
    }

    public function DataPanel(Request $request){
      $tgl = $request->input('tgl');
      $data = ApiCurl::panelTraceDoc($tgl);
      return response($data);
    }
}