<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use ApiCurl;

class ReturnController extends Controller
{
    protected $layout = "master.template";

    public function ToWH(){
        return view('content.towh');
    }

    public function ToHO(){
        return view('content.toho');
    }

    public function ToBilling(){
        return view('content.tobilling');
    }

    public function updateRtnWH(Request $request){
        $awb = $request->input('waybillno');
        $rtnwh = $request->input('rtnwhdate');
        $rtnho = $request->input('rtnhodate');
        $doc = $request->input('docreturn');
        $remark = $request->input('keterangan');
        $user = $request->input('userid');
        $hcpod = $request->input('statuspod');
        
        $data = ApiCurl::postRtnToWh($awb, $rtnwh, $rtnho, $doc, $remark, $hcpod, $user);
        return response($data);
        
    }

    // public function updateRtnHO(Request $request){
    //     $hawb = $request->input('rtntoho_awb');
    //     $tgl_rtn = $request->input('tgl_rtnho');
    //     $doc = $request->input('docho');
    //     $remark = $request->input('remarkho');
    //     // $hcpod = $request->input('select_hcpodho');
    //         if(ReportModels::where('hawb_no', '=', $hawb)->exists())
    //         {
    //             $data = array('rtndateHo'=>$tgl_rtn, 'doctoho'=>$doc, 'remarkho'=>$remark);

    //             ReportModels::updateData($hawb, $data);
    //             return json_encode(['status'=>200]);
    //         }
    //         else
    //         {
    //             return json_encode(['status'=>400]);
    //         }
        
    // }

    public function ApiAWB_WH(Request $request){
        $awb = $request->input('rtntowh_awb');
        $data = ApiCurl::getAwb($awb);
        foreach($data as $i){   
            if($i != null){
                return response()->json(['status' => 200, 'data' => $i]);
            }
        }
        return response()->json(['status' => 400]);
    }

    public function ReportAwb(Request $request){
        $awb = $request->input('awb');
        $data = ApiCurl::getReportAwb($awb);
        foreach($data as $i){   
            if($i != null){
                return response()->json(['status' => 200, 'data' => $i]);
            }
        }
        return response()->json(['status' => 400]);
    }

    public function ApiGetBillingDatatable(Request $request){
        $tgl = $request->input('billingdate');
        $user = "all";
        $data = ApiCurl::getBilling($tgl, $user);
            if($data != null){
                return DataTables::of($data)
                ->addColumn('action', function($data){
        
                    return '<button type="button" name="detail" id="'.$data['hawb_no'].'" class="detailRtnBilling btn btn-default btn-sm"><i class="fa fa-info-circle" aria-hidden="true"></i></button>';
                })
                ->addColumn('checkbox', function($data){
                    return '<input type="checkbox" id="'.$data['hawb_no'].'" value="'.$data['hawb_no'].'" class="cbk_rtnbilling">';
                })
                ->rawColumns(['action', 'checkbox'])
                ->make(true);
                return response($data);
            } 
        return response(array(
            "data" => "",
        )); 
    }

    public function ApiGetBilling(Request $request){
        $tgl = $request->input('billingdate');
        $user = $request->input('select_user');
        $data = ApiCurl::getBilling($tgl, $user);
      
        return response($data);
    }

    public function ApiRtnBilling(Request $request){
        $awb = $request->input('awb');
        $tgl = $request->input('tgl');
        $doc = $request->input('doc');
        $ket = $request->input('ket');
        $userid = $request->input('userid');
        $hcpod = $request->input('hcpod');
        $data = ApiCurl::postRtnBilling($awb, $tgl, $doc, $ket, $userid, $hcpod);
            if($data != null){
                return response($data);
            } 
            return response(array(
                "data" => "",
            )); 
    }
}