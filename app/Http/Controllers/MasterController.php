<?php

namespace App\Http\Controllers;

use ApiCurl;
use DataTables;
use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function home(){
        return view('welcome');
    }

    public function kecamatan($periode, $citykode){
        $data = ApiCurl::get_kec($periode, $citykode);
        return view('content.kecamatan', ['dataKec' => $data, 'periode' => $periode]);
    }

    public function kelurahan($periode, $citykode, $distcode){
        $data = ApiCurl::get_kel($periode, $citykode, $distcode);
        return view('content.kelurahan', ['dataKel' => $data, 'citycode' => $citykode, 'periode' => $periode]);
    }

    public function getKabupaten(Request $request){
        $periode = $request->input('periode');
        
        $data = ApiCurl::get_kab($periode);
  
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $button = '<button type="button" name="info" id="'.$data['citykode'].'" class="detailKab btn btn-inverse btn-sm"><i class="ti-info"></i></button>';
            return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
        
        return response($data);
    }

    public function getKecamatan(Request $request){
        $periode = $request->input('periode');
        $citykode = $request->input('citykode');
        
        $data = ApiCurl::get_kec($periode, $citykode);
  
        return DataTables::of($data)
        ->addColumn('action', function($data){
            $button = '<button type="button" name="info" id="'.$data['districtkode'].'" class="detailKec btn btn-inverse btn-sm"><i class="ti-info"></i></button>';
            return $button;
        })
        ->rawColumns(['action'])
        ->make(true);
        
        return response()->json(['data' => $data]);
    }

    public function getKelurahan(Request $request){
        $periode = $request->input('periode');
        $citykode = $request->input('citykode');
        $distcode = $request->input('distcode');
        
        $data = ApiCurl::get_kel($periode, $citykode, $distcode);
  
        return DataTables::of($data)
        ->make(true);
        
        return response()->json(['data' => $data]);
    }

    public function getPanelKab(Request $request){
        $periode = $request->input('periode');
        
        $i = ApiCurl::get_panel_kab($periode);
                
        return response()->json(['data' => $i]);
    }

    public function getPanelKec(Request $request){
        $periode = $request->input('periode');
        $citykode = $request->input('citykode');
        
        $i = ApiCurl::get_panel_kec($periode, $citykode);
                
        return response()->json(['data' => $i]);
    }
}