<?php

namespace App\Http\Controllers;

use ApiCurl;
use Illuminate\Http\Request;

class ReceivedController extends Controller
{
    protected $layout = "master.template";

    public function recBilling(){
        return view('content.recbilling');
    }

    public function ApiRecBilling(Request $request){
        $awb = $request->input('awb');
        $tgl = $request->input('tgl');
        $doc = $request->input('doc');
        $ket = $request->input('ket');
        $userid = $request->input('userid');
        $hcpod = $request->input('hcpod');
        $data = ApiCurl::postRecBilling($awb, $tgl, $doc, $ket, $userid, $hcpod);
            if($data != null){
                return response($data);
            } 
            return response(array(
                "data" => "",
            )); 
    }
}