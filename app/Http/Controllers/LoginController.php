<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use ApiCurl;

class LoginController extends Controller
{ 
    // public function tes(){
    //     return view('button');
    // }
       
    public function showLoginForm(){
        return view('login');
    }
    
    public function username(Request $request){
        $user = $request->input('username');
        $data = ApiCurl::get_username($user);
        return $data;
    }

    public function PostLogin(Request $request){
        $user = $request->input('username');
        $pass = $request->input('password');
        $data = $this->username($request);
        foreach($data as $i){   
            if($i != null){
                if ($user == $i['usrId'] && $pass == $i['usrPass']) {
                    Session::put('id',$i['usrId']);
                    Session::put('username',$i['usrName']);
                    Session::put('login',TRUE);
                    // return response()->json([
                    //     'success' => 200,
                    //     'data' => $i,
                    // ]);
                    return redirect('/');
                } else {
                    // return response()->json([
                    //     'success' => 500,
                    //     'message' => 'Login Gagal!'
                    // ]);
                    return redirect('/login')->with(['error' => 'Username atau Password salah']);
                }
            }
        }
        // return response()->json([
        //     'success' => 404,
        //     'message' => 'Data tidak ditemukan!'
        // ]);
        return redirect('/login')->with(['error' => 'Username tidak ditemukan!']);
    }

    public function Logout(){
            Session::flush();
            return redirect('/login');
    }
}