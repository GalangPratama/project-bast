@extends('master.template')

@section('css')
    <link href="{{ url('plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="{{ url('plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
                <!-- Page-Title -->
                <div class="row">
                    <div class="col-lg-6 col-xl-6 NameKec">
                        <div class="page-title-box">
                            <h4 class="page-title">
                                @foreach($dataKec as $item)
                                    @if ($loop->first)
                                        <h4 class="page-title">{{ $item['cityname'] }}</h4>
                                    @endif
                                @endforeach
                            </h4>
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-6 BtnBack" style="text-align: right;">
                        <div class="page-title-box">
                           <button class="btn btn-inverse" id="button-BACK">
                            <i class="ti-arrow-left"></i>  BACK
                           </button>
                        </div>
                    </div>
                </div>
                <!-- end page title end breadcrumb -->


                <div class="row">
                    <div class="col-lg-12 col-xl-4">
                        <div class="card-box widget-box-1 bg-danger">
                            <i class="fa fa-info-circle text-white pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                            <h4 class="font-18">Belum ada BAST</h4>
                            <h2 class="text-center"><span data-plugin="counterup" id="panelMERAH"></span></h2>
                            <p class="text-white" id="ttlbastMERAH"></p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-xl-4">
                        <div class="card-box widget-box-1 bg-warning">
                            <i class="fa fa-info-circle text-white pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                            <h4 class="font-18">BAST dalam Proses</h4>
                            <h2 class="text-center"><span data-plugin="counterup" id="panelKUNING"></span></h2>
                            <p class="text-white" id="ttlbastKUNING"></p>
                        </div>
                    </div>

                    <div class="col-lg-12 col-xl-4">
                        <div class="card-box widget-box-1 bg-success">
                            <i class="fa fa-info-circle text-white pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                            <h4 class="font-18">BAST sudah Kembali</h4>
                            <h2 class="text-center"><span data-plugin="counterup" id="panelHIJAU"></span></h2>
                            <p class="text-white" id="ttlbastHIJAU"></p>
                        </div>
                    </div>
                </div>

                
                        <div class="row card-box table-responsive">
                            {{-- <div class="col-lg-12 col-xl-6 select_periode">
                                <select name="periode" id="periode" class="input-form">
                                    <option selected="selected" value="I">Periode 1</option>
                                    <option value="II">Periode 2</option>
                                    <option value="III">Periode 3</option>
                                </select>
                            </div> --}}

                            <div class="col-lg-12 col-xl-12 search">
                                <input type="text" class="form-input" name="search-table" id="search-table" placeholder="Search">
                            </div>
                            <table id="datatable-kecamatan" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Periode</th>
                                    <th class="text-center">Belum ada BAST</th>
                                    <th class="text-center">BAST dalam proses</th>
                                    <th class="text-center">BAST sudah kembali</th>
                                    <th class="text-center">Total BAST Sudah Kembali (%)</th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                        @foreach ($dataKec as $item)
                            
                        @endforeach
@endsection


@section('js')
        <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Responsive examples -->
        <script src="{{ url('plugins/datatables/dataTables.responsive.min.js') }}"></script>
        <script src="{{ url('plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                var APP_URL = {!! json_encode(url('/')) !!}
                var periode = '{{ $periode }}';
                var citykode = '{{ $item['citykode'] }}';
                console.log(periode);
                console.log(citykode);

                $.ajax({
                    url: APP_URL+"/data/panel/kec",
                    method: "GET",
                    data: {periode:periode, citykode:citykode},
                    dataType: "json",
                    success:function(res){
                        var data = res.data;

                        $.each(data, function(i, val){
                            var merah = val.merah;
                            var kuning = val.kuning;
                            var hijau = val.hijau;
                            var jmlkel = val.jmlkel;

                            var persentMRH = (merah/jmlkel)*(100);
                            var persentMRH = persentMRH.toFixed(2);
                            $('#panelMERAH').html(persentMRH+'%');
                            $('#ttlbastMERAH').html('TOTAL BAST : '+merah);

                            var persentKNG = (kuning/jmlkel)*(100);
                            var persentKNG = persentKNG.toFixed(2);
                            $('#panelKUNING').html(persentKNG+'%');
                            $('#ttlbastKUNING').html('TOTAL BAST : '+kuning);

                            var persentHJU = (hijau/jmlkel)*(100);
                            var persentHJU = persentHJU.toFixed(2);
                            $('#panelHIJAU').html(persentHJU+'%');
                            $('#ttlbastHIJAU').html('TOTAL BAST : '+hijau);
                        })
                    }
                });

                var tableKec = $('#datatable-kecamatan').DataTable({
                    processing: true,
                    serverside: true,
                    searching: true,
                    ordering: false,
                    destroy: true,
                    dom: 'tp',
                    columnDefs:[
                        {
                            "className": "text-center", 
                            "targets": [2, 3, 4, 5, 6]
                        }
                    ],
                    ajax: {
                        method : 'GET',
                        url : APP_URL+"/data/kec",
                        dataType : 'json',
                        data : {periode:periode, citykode:citykode},
                    },
                    columns: [
                        {
                            data:'action'
                        },
                        {
                            data:'districtname'
                        },
                        {
                            "data": null,
                            "render": function(data, type, meta){
                                return periode;
                            }
                        },
                        {
                            data:'merah'
                        },
                        {
                            data:'kuning'
                        },
                        {
                            data:'hijau'
                        },
                        {
                            "data": null,
                            "render": function(data, type, row){
                                var hijau = Math.round(data.hijau / data.ttlkel * 100).toFixed(2) +'%';
                                return hijau;
                            }
                        }
                    ],
                    rowCallback: function(row, data, index){
                        var percent = Math.round(data.hijau / data.ttlkel * 100).toFixed(2);
                        if (percent <= 20.00) {
                            $(row).find('td:eq(6)').css('background', '#fc2a2a');
                        } else if(percent <= 40.00) {
                            $(row).find('td:eq(6)').css('background', '#fb9043');
                        } else if(percent <= 60.00) {
                            $(row).find('td:eq(6)').css('background', '#fae05c');
                        } else if(percent <= 80.00) {
                            $(row).find('td:eq(6)').css('background', '#cff632');
                        } else{
                            $(row).find('td:eq(6)').css('background', '#47f209');
                        }                           
                    }
                }); 
                $('#search-table').keyup(function(){
                    tableKec.search($(this).val()).draw() ;
                })

                $(document).on('click', '.detailKec', function(){
                    var districtkode = $(this).attr("id");
                    location.href = APP_URL+"/kelurahan/"+periode+"/"+citykode+"/"+districtkode;
                });
                        
                $('#button-BACK').click(function() {
                    history.go(-1);    
                    return false;
                });
            });
        </script>
@endsection