<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>BAST - Total Logistics</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="shortcut icon" href="{{ url('assets/images/logo-total-sm.png') }}">

        <!--Morris Chart CSS -->
        <link rel="stylesheet" href="{{ url('plugins/morris/morris.css') }}">
          @yield('css')
        <!-- App css -->
        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ url('assets/js/modernizr.min.js') }}"></script>

    </head>


    <body>


        <!-- Navigation Bar-->
          @include('master.header')
        <!-- End Navigation Bar-->


        <div class="wrapper">
            <div class="container-fluid">

                @yield('content')

            </div> 
            <!-- end container -->
        </div>
        <!-- end wrapper -->


        <!-- Footer -->
          @include('master.footer')
        <!-- End Footer -->


        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>

        <script src="{{ url('plugins/morris/morris.min.js') }}"></script>
        <script src="{{ url('plugins/raphael/raphael-min.js') }}"></script>

        <script src="{{ url('assets/pages/jquery.dashboard_4.js') }}"></script>

          @yield('js')

        <!-- App js -->
        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>
    </body>
</html>