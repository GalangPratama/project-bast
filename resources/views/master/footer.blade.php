<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center text-dark">
                Copyright © 2020 by <a href="#">TORA Development Team</a>
            </div>
        </div>
    </div>
</footer>