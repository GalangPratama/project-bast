<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ url('assets/images/logo-total-sm.png') }}">

        <title>Login - Trace Document TICS</title>

        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.all.min.js"></script>  
        <script src="{{ url('assets/js/modernizr.min.js') }}"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading text-center">
                    <img src="{{ url('assets/images/logo-total.png') }}" alt="" width="250">
                </div>


                <div class="p-20">
                    <form class="form-horizontal" method="GET" action="{{ url('/proses') }}">
                        {{-- {{ csrf_field() }} --}}
                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-6 control-label">Username</label>

                            <div class="col-md-12">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" autofocus autocomplete="off">

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>                        
                        <div class="form-group text-center m-t-40">
                            <div class="col-12">
                                <button id="logintracedoc" class="btn btn-default btn-block text-uppercase waves-effect waves-light"
                                        type="submit">Log In
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
        
        <div id="loading" style="display: none;"> 
            <div class="loader">
                <img src="{{ url('assets/images/sp-loading2.gif') }}" alt="loading">
            </div>
        </div>

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/detect.js') }}"></script>
        <script src="{{ url('assets/js/fastclick.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/wow.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>

        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>
        <script>
             $(document).ready(function() {
                // var APP_URL = {!! json_encode(url('/')) !!}
                // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                // $('#logintracedoc').click(function(){
                //     $(document).ajaxStart(function() {
                //         $("#loading").show();
                //         }).ajaxStop(function() {
                //         $("#loading").hide();
                //     });
                //     var username = $('#userLogin').val();
                //     var password = $('#userPassword').val(); 
                //     if (username != '' && password != '') {
                //         $.ajax({
                //         url: APP_URL+'/proses',
                //         method:'GET',
                //         data:{'username':username, 'password':password},
                //         dataType:'json',
                //         success:function(res)
                //         {
                //             if (res.success == 200) {
                //                 window.location.href = "{{ url('/') }}";
                //                 console.log(res);
                //             } else {
                //                 $('#errorLogin').html(res.message);
                //             }
                //         }
                //     })
                //     } else {
                //         swal("", "Masukan Username dan Password", "warning");
                //     }
                // })
            });
        </script>
	
	</body>
</html>