<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <link rel="shortcut icon" href="{{ url('assets/images/logo-total-sm.png') }}">

        <title>Login - Trace Document TICS</title>

        <link href="{{ url('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ url('assets/css/style.css') }}" rel="stylesheet" type="text/css" />

        <script src="{{ url('assets/js/modernizr.min.js') }}"></script>
        
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="card-box">
                <div class="panel-heading text-center">
                    <img src="{{ url('assets/images/logo-total.png') }}" alt="" width="250">
                </div>


                <div class="p-20">
                    <form class="form-horizontal">
                        {{ csrf_field() }}

                        {{-- <div class="form-group{{ $errors->has('usrId') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-6 control-label">Username</label>

                            <div class="col-md-12">
                                <input id="usrId" type="text" class="form-control" name="usrId" value="{{ old('usrId') }}" required autofocus>

                                @if ($errors->has('usrId'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('usrId') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('usrPass') ? ' has-error' : '' }}">
                            <label for="usrPass" class="col-md-12 control-label">Password</label>

                            <div class="col-md-12">
                                <input id="usrPass" type="password" class="form-control" name="usrPass" required>

                                @if ($errors->has('usrPass'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('usrPass') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div> --}}

                        
                    </form>
                    <div class="form-group text-center m-t-40">
                        <div class="col-12">
                            <a href="{{ url('/proses?username=sisca&password=nhauq719') }}">
                                <button id="logintracedoc" class="btn btn-default btn-block text-uppercase waves-effect waves-light"
                                        type="submit">Log In
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        

        
    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ url('assets/js/jquery.min.js') }}"></script>
        <script src="{{ url('assets/js/popper.min.js') }}"></script><!-- Popper for Bootstrap -->
        <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/detect.js') }}"></script>
        <script src="{{ url('assets/js/fastclick.js') }}"></script>
        <script src="{{ url('assets/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.blockUI.js') }}"></script>
        <script src="{{ url('assets/js/waves.js') }}"></script>
        <script src="{{ url('assets/js/wow.min.js') }}"></script>
        <script src="{{ url('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ url('assets/js/jquery.scrollTo.min.js') }}"></script>

        <script src="{{ url('assets/js/jquery.core.js') }}"></script>
        <script src="{{ url('assets/js/jquery.app.js') }}"></script>
        <script>
            $(document).ready(function() {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                // $('#logintracedoc').click(function(){
                //     var username = $('#username').val();
                //     var password = $('#password').val(); 

                //     if (username != '') {
                //         $.ajax({
                //         url:"/ceklogin",
                //         method:"POST",
                //         data:{'username':username, 'password':password},
                //         dataType:'json',
                //         success:function(data)
                //         {
                //             // $.each(data, function(i, val) {
                //                 console.log(data);
                //             // })  
                //         }
                //     })
                //     } else {
                //         alert('DATA HARUS DI ISI');
                //     }
                // })
            });
        </script>
	
	</body>
</html>